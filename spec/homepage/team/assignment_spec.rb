describe Gitlab::Homepage::Team::Assignment do
  let(:member) { double('member') }
  let(:project) { double('project') }

  subject do
    described_class.new(member, project, 'maintainer backend')
  end

  describe '#responsibility' do
    it { expect(subject.responsibility).to eq 'maintainer' }
  end

  describe '#description' do
    it { expect(subject.description).to eq 'backend' }
  end

  describe '#member' do
    it { expect(subject.member).to eq member }
  end

  describe '#project' do
    it { expect(subject.project).to eq project }
  end

  describe '#owner?' do
    it { expect(subject).not_to be_owner }
  end

  describe '#maintainer?' do
    it { expect(subject).to be_maintainer }
  end

  describe '#reviewer?' do
    it { expect(subject).not_to be_reviewer }
  end

  describe '#username' do
    it 'delegates username to member' do
      expect(member).to receive(:username).once

      subject.username
    end
  end
end
