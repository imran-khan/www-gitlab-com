---
layout: markdown_page
title: "Idea to Production Demo"
---

## Video
{:.no_toc}

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/PoBaY_rqeKA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Overview
{:.no_toc}

Today, modern software development teams need version control for everything, automated testing, support for complex build and deploy configurations, and end-to-end visibility and traceability so they can work to improve their software development process over time. But for most teams, getting this tooling right is incredibly difficult.

GitLab provides the most efficient platform for software development and delivery, covering the entire lifecycle from idea to production.

This demonstration will highlight [GitLab’s single platform for the full software development lifecycle](https://about.gitlab.com/direction/#scope), from idea to production, through chat, issues, planning, merge request, CI, CD, measurement, and monitoring.

![](handbook/sales/lifecycle.png)

We want to to make sure [everyone can replicate this demo](https://gitlab.com/gitlab-org/gitlab-ce/issues/25986).
We've changed this page to make it work with Google Container Engine (GKE) instead of OpenShift.
If you encounter issues replicating this demo on GKE or on your own Kubernetes cluster please [open an issue](https://gitlab.com/gitlab-org/kubernetes-gitlab-demo/issues/new).
We're still working to improve this demo further, please see [all open idea-to-production issues](https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=idea-to-production).

## Table of Contents
{:.no_toc}

- TOC
{:toc}

## Set up

### Installation

There are three options:

1. [Log in at gitlab.i2p.online](https://gitlab.i2p.online) (for GitLab sales people)
1. [Set up a cluster on Google Container Engine (GKE)](gke-setup)
1. [set up a cluster on Azure Container Service (ACS)](acs-setup)

### Cleanup

> * Delete previous GitLab groups you made last time
> * Delete previous Kubernetes namespace for projects (`kubectl delete namespace minimal-ruby-app-<initials>-<project-id>`)
> * Delete previous Mattermost team or at least leave Mattermost team

### Create a user

*Note: If using a shared demo, each demo-giver only needs to do this once.*

Now let's register a new user in our GitLab server.

> * Click `Register`
> * Create a user with your name and email address (no verification sent)

### Create a group

We've got GitLab running and we're logged in. Since we'll want to work as a team, we'll create a GitLab group. Groups allow you to organize projects into directories and quickly gives users access to several projects at once. With GitLab 9.0, you can even nest subgroups under groups to match your org structure. Let’s give ours a unique name.

> * Click hamburger menu in top-left corner > Groups
> * Click `New Group`
> * Give the group a unique name, perhaps the name of the company you're demoing to, or a made up name (all lowercase, no spaces or special characters other than `-`)
> * Change Visibility level to `Public`
> * Leave `Create a Mattermost team for this group` checked
> * Click `Create group`

### Create a project

*Note: If you've given the demo before, make sure to delete the `minimal-ruby-app` project first.*

Now let’s create a new project. I'll start from a really simple example app just to save myself some typing.

> * Click `New Project`
> * Set `Project name` to `minimal-ruby-app-<initials>` (must be unique on instance)
> * Click `Repo by URL`
> * Set `Git repository URL` to `https://gitlab.com/gitlab-examples/minimal-ruby-app.git`
> * Make it public

### Set up Mattermost Command

Now let's get our project connected to the built-in Mattermost. Mattermost is an open source Slack alternative that comes bundled with GitLab. We also integrate with Slack so you could use that as your chat client as well.

> * Select Mattermost Slash Commands
> * Click Add to Mattermost
> * Pick company name from list
> * Click Install

### Setup GitLab Auto-Deploy

Now we’re ready to configure GitLab Auto Deploy. Auto Deploy is the easiest way to configure GitLab CI from a template. Back to the project, let’s click `Set up auto deploy` and choose the Kubernetes template. This is a great template to get us started and we just need to edit the `KUBE_DOMAIN` to use our own domain.

> * Go to Project, Click `Set up auto deploy`
> * Choose Kubernetes template
> * Edit the template
>   * Change `KUBE_DOMAIN` from `domain.example.com` to `i2p.online` (or the base domain you are using)
> * Change Target Branch field to `master`
> * Commit

Great, that completes our setup.

## Project permissions (optional: only show if project permissions are important)

Okay, so everything we need to bring an application from idea to production is set up. But let's assume you want to safeguard your source code before handing this over to your developers. I'll take you through a few key ways you can outline project permissions and manage your team's workflows.

**User roles and permissions**: Since this is a public project, we’ll want to ensure that we have a way to manage what actions each team member can take. For example, we may want only certain people to be able to merge to `master` or to be able to adjust the CI project configuration.

**Change a user’s permission level**: In GitLab, permissions are managed at a user and group level and they apply to projects and GitLab CI. We have five different role types, so you can set granular permissions and keep your code and configurations management secure. To save your admins time and the headache of managing multiple logins, GitLab integrates with your Active Directory and LDAP. You can just connect GitLab to your LDAP server and it will automatically sync users, groups, and admins.

**Project settings**: In addition to permissions, we also have features to help you manage the team’s workflow and bake quality control into your development process.

**Navigate to project settings for protected branches**: It’s no secret that code review is essential to keeping code quality high. But when the team is on a deadline, there could be an incentive to skip code review and force-push changes. Therefore, in addition to permissions, we also allow you to identify protected branches to prevent people from pushing code without review. The `master` branch is protected by default but you can easily protect other branches, like your QA branch, and restrict push and merge access to certain users or groups.

**Navigate to project settings for merge request approvals**: If you want to take code review a step further and ensure your code is always reviewed and signed off by specific team members, you can enable merge request approvals. GitLab lets you set the number of necessary approvals and predefine a list of approvers or approval groups that will need to approve every merge request in the project.

Permissions, merge request approvals, and protected branches help you build quality control into your development process so you can confidently hand GitLab over to your developers to get started on turning their ideas into a reality.

## Idea (Chat)

Let's go to our Mattermost client. Today, more of your team's conversations are happening in chat, not in issues. With GitLab's chat command integration, you can easily turn ideas into issues.

> * Go to Mattermost `https://mattermost.i2p.online` (Change the domain to match the domain used for your GitLab installation)
> * Skip tutorial
> * Type: `/minimal-ruby-app help`

On first use, the command will ask you to connect your GitLab account, which is as simple as clicking the provided link in the response.

> * Click connect your GitLab account
> * Click Authorize
> * Go to Mattermost
> * Type: `/minimal-ruby-app help`

Great. Now we can see what commands are available. Let's go ahead and create an issue.

> ```
> /minimal-ruby-app issue new Make homepage more descriptive
> SHIFT ENTER
> Currently it is just Hello World.
> ```

## Issue (Tracker)

Great, we’ve created a new issue, directly from chat, and now we can click through to see our first issue on our new project.

> * Click on the link that starts with #1

## Plan (Board)

Inspiration is perishable, so let's pick this one up right away. As a team lead or manager, I'd go to the Issue Board.

> Go to Issues > Board

Since this is our first time, we have to add a couple columns here to match our workflow. These columns are fully-customizable and you can have multiple Issue Boards per project to help your team organize their releases. I'll just add the default "To Do" and "Doing" columns.

> * Add default lists

There. Now we can just add the new issue from the backlog into the Doing column, because we want to resolve this issue right now.

> * Click `Add issue`
> * Select the issue (click on the whitespace in the box; not the issue title)
> * Click on `Add 1 issue`
> * Drag issue from `To Do` to `Doing`

## Commit (Repo)

Now let’s get coding! I'll head over to the Repository and start editing.

> * Go to Repository
> * Go to `server.rb`
> * Click Edit button
> * Add `Updated ` in front of  `Hello, world!`
> DON'T COMMIT

Now instead of committing directly to `master`, I’m going to create a new branch, named with the issue number.

> * Set target branch to `1-homepage` (no longer than 24 characters)
> * Leave start a new merge request checked
> * Commit

And now it gives me an option to create a Merge Request, how nice of it. Let's go ahead and do that. GitLab knows by the branch name that it closes issue #1 and adds that message automatically so we don't have to do anything except hit submit.

> * Check `Remove source branch when merge request is accepted.`
> * `Submit merge request`
> * If popup asks to show notifications, click Allow.

## Build (CI)

As soon as the Merge Request is created, we see it automatically kicked off the CI/CD Pipeline that will test our contributed code.

> * Click on Pipelines
> * Click on first (top) pipeline's status.

Here we see a simple pipeline that contains 3 stages for build, review, and cleanup. In the build step, we build the Docker container image.

> * Click on Build

## Runner progress (optional: only show if CI/CD is taking a while)

While it’s running, we can head back to our Kubernetes console to see that our GitLab Runner is working directly with Kubernetes to spawn new containers for each job, as they are needed. It even creates a namespace for the project, providing isolation.

> * Go to Kubernetes
> * Change Namespace to `default`
> * Click on Pods
> * Change the Namespace drop-down to `minimal-ruby-app`
> * Click on Pods

## Review (MR)

### Review apps

And in the Review step, we deploy that image to a temporary review app in our Kubernetes cluster.
> * Go Back
> * Click on Review

Code review is great, but I don’t just want to trust reading the code, I want to see it live in a production-like environment and this review app provides that.

> * Back, Back (Go back twice, to the MR)

Right from the merge request, we see a new status telling us that it’s been deployed, and a convenient link to the actual app. Let’s take a look.

* Click on external link to review app

So this is what we just changed, and any new changes pushed to our branch will automatically update the app.

### Debugging (Terminal)

Now if there were any problems, for example differences between development and production, and you don't want to keep testing changes by pushing them to source control, we could debug those problems right here. By clicking the web terminal button we get a command prompt in the same container as our application.

> * Close review app
> * Click on `review/1-homepage`
> * Click Terminal button (on the upper right, 1st on right)

All our files are here. Let's edit the server.rb file.

> * `ls`
> * `vi server.rb`
> * i (to insert)
> * Update text to `Corrected Hello World!`
> * esc (to go back to normal mode)
> * ZZ (to save and close)

Now we’ve saved the changes, let's restart the server.

> * `killall ruby`

And now we can view the web page live to see how we like the changes.

> * Click external URL link on top right (2nd from right)

### Code review

At this point we'd usually ask for another developer on the team to review our merge request. They can see the exact code that has changed, comment on it, and we'd see a thread of the discussion, as well as get an email notification, of course.

> * Go back to the Merge Request and go to the tab Changes
> * Click on a change line to show ability to comment
> * Comment "Looks good", Submit
> * Go to Discussion tab to see comment

### Merge to `master`

This all looks great so let’s click the Accept Merge Request button to merge the changes into the master branch.

> * Click Accept Merge Request

## Canary

Now let’s ship these changes to production! But we’re not going to ship directly to the entire production fleet. In GitLab 9.1, we added support for canary deploys, basically letting you deploy to a smaller portion of your fleet to reduce risk.

> * Environments
> * Staging -> Canary

But now we see something interesting, I’ll keep hitting refresh, and you can see that half the time, I get the new canary code, and half the time I get the old code.

## Production (Chatops)

When we’ve validated that the canary is working as expected, we can then go and ship it completely to production. ChatOps encourages us to do these kinds of things in a chat room, so everyone can see important changes. So let's go back to our chat room where this all started, and tell GitLab to deploy whatever’s on staging to production.

> *Note: You have to wait for the staging environment to deploy before this will work.*
> * Go to Mattermost
> * Type `/minimal-ruby-app deploy staging to production`
> * Click on the link

Great, here we see the deploy to production happening live.

> * wait until it is done

### Environments with deployment history

Now that it is done let’s go back to Environments.

> * Go to Environments

Ok great, we now see the production deploy happened less than a minute ago.

> * Click external URL link (left-most button)

There we go! We've got our new text in it; all the way from idea to production!

## Deploy Boards (optional: only show if you demoing GitLab EE)

Here's a new feature in 9.0, if I click on this icon, I see the deploy board for `production`. Right now it's only showing a single pod, but a new tweak to Auto Deploy is that I can control the size of my production fleet using project variables. Here I’ll set `PRODUCTION_REPLICAS` to 10 and redeploy.

* Settings -> Pipeline
* PRODUCTION_REPLICAS=10
* Environments
* Redeploy

And here we can see fleet get rolled out in realtime.

## Monitoring

So that's a high level status of the deploy, but how about monitoring the ongoing health of your app environments? Clicking on the graph icon, I see a graph of CPU and memory usage, taken from the built-in Prometheus monitoring, the leading open-source monitoring solution. There’s not much to see right now, but this will show the last 8 hours so you can monitor how your app is doing. Application performance monitoring can help your team be more strategic, preventing errors vs. simply reacting to them. Imagine if your application monitoring tool could help you avoid pushing poor-performing code in the first place, saving your business future downstream costs? That's exactly where we are heading.

> * Go to Environments > `staging`
> * Click on graph icon

### Merge request monitoring

Going back to the merge request...

> * Back, back

We now see another status showing that this code has indeed been deployed to staging and production. In GitLab 9.2, we introduced app performance feedback, right on the merge request, and in 9.3, we made it even better, telling you how much your memory usage changed before and after the merge request was deployed.

## Feedback (Cycle Analytics)

To help you spot bottlenecks in your development process, GitLab has a built-in dashboard that tracks how long it takes the team to move from idea to production.

> * Click Project > Cycle Analytics

Here we can see some metrics on the overall health of our project, and then a breakdown of average times spent in each stage on the way from idea to production. So far, we're doing amazingly well, by completing a release cycle in minutes.

This is great for team managers and high level managers looking to better understand their company's release cycle time, which is key to staying competitive and responding to customers.

### Instance Monitoring (optional)

Not only does Prometheus monitor your apps, but it monitors the GitLab instance itself. Let's go to the Prometheus dashboard.

> * Visit Prometheus `https://prometheus.i2p.online`  (Change the domain to match the domain used for your GitLab installation)

Let’s look at a couple simple queries that show how your GitLab instance is performing. Here’s our CPU usage:

> * Copy `1 - rate(node_cpu{mode="idle"}[5m])` into the Expression bar; hit enter.
> * Click Graph

And then memory usage:

> * Copy `(1 - ((node_memory_MemFree + node_memory_Cached) / node_memory_MemTotal)) * 100` into the Expression Bar; hit enter.

## Conclusion

So that's it. In less than 10 minutes, we took an an idea through issue tracking, planning with an issue board, committing to the repo, testing with continuous integration, reviewing with a merge request and a review app, debugging in the terminal, deploying to production with ChatOps, application performance monitoring, and closing the feedback loop with cycle analytics dashboard. This all on top of a container scheduler that allows GitLab, the GitLab Runners for CI, and the applications we deploy to scale. Welcome to GitLab, the only platform that connects every step of your software development lifecycle, helping you bring modern applications from idea to production, quickly and reliably.
