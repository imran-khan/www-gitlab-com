---
layout: markdown_page
title: "Frontend Group"
---

## Teams

There are two teams within the Frontend group: AC and DC. Both groups
responsibiltiies map to [teams on the Backend group](https://about.gitlab.com/handbook/backend/).
In addition, AC also covers [frontend marketing](#marketing). The current mapping
is as follows:

* AC (Platform, Prometheus, Marketing)
* DC (Discussion and CI/CD)

There is a frontend group call every Tuesday, before the team call. You should have been invited when you joined; if not, ask your team lead!

[Find the product manager mapping to engineering teams in the product handbook](/handbook/product)

### Frontend Marketing
{: #marketing}

The Frontend Marketing team is responsible for all frontend content on the [about.gitlab.com](about.gitlab.com) website. This website is our homepage. It presents tons of different marketing material to many different users. On this site you will see:

* Our team page.
* Our blog.
* Ways to sign up for GitLab.
* Different talks GitLabbers are giving.
* Product comparisons.
* etc.

Marketing Frontend Engineers will be responsible for the overall looks of this site with the help of UX.

### Choosing something to work on

Prior starting your work on your `Deliverable` labeled issues take a look at the `Next Patch Release ` [issue list](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=regression&label_name[]=Next%20Patch%20Release&label_name[]=Frontend).
