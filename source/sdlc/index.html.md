---
layout: markdown_page
title: Software Development Life Cycle (SDLC)
---

## SDLC definition

The modern Software Development Life Cycle (SDLC) consists multiple phases. It starts with chatting about an idea and ends at measuring the metrics of running it in production. Below is a list of the phases with the type of tool used for this in brackets.

1. Idea (Chat)
1. Define (Issue Tracker)
1. Plan (Issue Board)
1. Code (Version Control)
1. Test (Continuous Integration)
1. Deploy (Continuous Delivery)
1. Measure (Metrics Monitoring)

## Most important organizational process

[Every company is becoming a software company.](https://www.forbes.com/sites/techonomy/2011/11/30/now-every-company-is-a-software-company/)
Therefore the SDLC is becoming the most important organizational process.
Effective software development is an essential skill to create value, attract great people, and keep applications secure.
To enable this skill organizations are adopting SDLC stacks that help this workflow.

## SDLC Stacks

There are a couple of organizations that are building a stack for the SDLC.

| Phase    | GitLab         | GitHub               | Atlassian                                       | Legacy Open Source      |
|----------|----------------|----------------------|-------------------------------------------------|-------------------------|
| Chat     | Mattermost     | Slack                | HipChat                                         | IRC                     |
| Define   | GitLab Issues  | GitHub Issues        | JIRA                                            | Redmine                 |
| Plan     | GitLab Boards  | GitHub Boards        | Trello                                          | Kanboards               |
| Create   | GitLab         | GitHub               | BitBucket                                       | SVN                     |
| Review   | GitLab CI      | Travis CI            | BitBucket CI / Bamboo                           | Jenkins                 |
| Release  | GitLab CD      | Heroku               | BitBucket CD                                    | Jenkins                 |
| Monitor  | GitLab Metrics | New Relic            | Undefined                                       | Nagios                  |


| Subject                                    | GitLab         | GitHub                | Atlassian                                       |
|--------------------------------------------|----------------|-----------------------|-------------------------------------------------|
| Preferred platform                         | Kubernetes     | Heroku                | n/a                                             |
| Single tenant install (self-hosted)        | Integrated     | Needs other products  | Separate products, CI/CD not actively developed |
| Multi tenant install (SaaS)                | Integrated     | Needs other products  | Includes CI/CD, issues in JIRA, no monitoring   |

## Cloud Native work-flow

Cloud Native means developing applications to run in the cloud.
The platform for deploying these applications is switching from Virtual Machines (AWS) to Container Schedulers (Kubernetes).
Cloud native applications are split up into [micro services](https://martinfowler.com/articles/microservices.html).
This means one application consists of many services that each have their own project and code base.
To handle these cloud native work-flows GitLab has [sub-groups](https://docs.gitlab.com/ce/user/group/subgroups/), [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/2517), and [multi-project pipelines](https://gitlab.com/gitlab-org/gitlab-ee/issues/933)

## An integrated product brings emergent benefits

GitLab is the only integrated product for the SDLC, all others are combinations of different products.
Having one product makes for a much better user experience because there is one UI, better security because of consistent permission settings, and less time spend on administration and integration.
Apart from that this 'development operating system' has some emergent properties that wouldn't otherwise be possible:

- [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/2517) with features such as Auto ChatOps and Auto Review Apps.
- [Cycle Analytics](https://about.gitlab.com/features/cycle-analytics/) that allow you to measure and reduce the time from idea to production.
- [ConvDev Index](https://gitlab.com/gitlab-org/gitlab-ce/issues/30469) to help spread best practices.

These emergent properties allow the following emergent benefits:

- Go from idea to value faster and more consistently.
- Creating better business outcomes, needing fewer people, having more security.
- Transforming the company from agile to [conversational development](http://conversationaldevelopment.com/).

## Open source is the future of software development

GitLab is developed out in the open with most code available under an open source license.
This allowed more than 1500 experts to contribute their process in the form of code.
GitLab distills the greatest collection of DevOps best practices into a cloud native work-flow.
Our integrated product allows you to stand on the shoulders of many experts.

## Related methodologies and software lifecycle concepts

- [Agile](http://agilemanifesto.org/)
- [DevOps](https://en.wikipedia.org/wiki/DevOps)
- [Conversational Development](http://conversationaldevelopment.com/) (ConvDev)
- [Application Lifecycle Management](https://en.wikipedia.org/wiki/Application_lifecycle_management) ALM is defined on wikipedia as "ALM is a broader perspective than the Software Development Life Cycle (SDLC), which is limited to the phases of software development such as requirements, design, coding, testing, configuration, project management, and change management. ALM continues after development until the application is no longer used, and may span many SDLCs."
- [Systems Development Life Cycle](https://en.wikipedia.org/wiki/Systems_development_life_cycle)
- Idea to Production (I2P) is a way to refer to the SDLC cycle of chatting about an idea to measuring it in production.
- [Remote Only](http://www.remoteonly.org/) organizations use SDLC tools to work effectively while they are distributed.